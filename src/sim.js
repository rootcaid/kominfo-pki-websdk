"use strict";
require("../lib/forge/asn1.js");
require("../lib/forge/util.js");
var Utils = require("./utils.js");

var asn1 = forge.asn1;

/**
 * Subject Identification Method RFC-4683
 */
var simValidator = {
  name: 'SIM',
  tagClass: asn1.Class.UNIVERSAL,
  type: asn1.Type.SEQUENCE,
  constructed: true,
  value: [
  {
    name: 'SIM.algoid',
    tagClass: asn1.Class.UNIVERSAL,
    type: asn1.Type.SEQUENCE,
    constructed: true,
    value: [{
      name: 'SIM.algo',
      tagClass: asn1.Class.UNIVERSAL,
      type: asn1.Type.OID,
      constructed: false,
      capture: 'algo'
    }]
  },
  {
    name: 'SIM.authorityRandom',
    tagClass: asn1.Class.UNIVERSAL,
    type: asn1.Type.OCTETSTRING,
    constructed: false,
    capture: 'authorityRandom'
  },
  {
    name: 'SIM.pepsi',
    tagClass: asn1.Class.UNIVERSAL,
    type: asn1.Type.OCTETSTRING,
    constructed: false,
    capture: 'pepsi'
  }
  ]
};


var SIM = function(der) {
  this.pepsi = null;
  this.authorityRandom = null;
  this.algo = '';
  this.der = null;

  if (der) {
    var capture = {};
    var errors = [];
    var asn1Data = asn1.fromDer(der);

    if (asn1.validate(asn1Data, simValidator, capture, errors)) {
      this.pepsi = forge.util.createBuffer(capture.pepsi);
      this.authorityRandom = forge.util.createBuffer(capture.authorityRandom);
      this.algoId = asn1.derToOid(capture.algo);
    }
  }
}

var createPepsi = function(sii, siiType, password, authorityRandom, algoId) {
  var hashContent = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [
    asn1.create(asn1.Class.UNIVERSAL, asn1.Type.UTF8, false, password),
    asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, authorityRandom.getBytes()),
    asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(siiType).getBytes()),
    asn1.create(asn1.Class.UNIVERSAL, asn1.Type.UTF8, false, sii),
  ]);
  var der = asn1.toDer(hashContent);

  var digestAlgorithmMap = {
    '1.3.14.3.2.26': 'sha-1',
    '2.16.840.1.101.3.4.2.1': 'sha-256',
    '2.16.840.1.101.3.4.2.2': 'sha-384',
    '2.16.840.1.101.3.4.2.3': 'sha-512'
  }

  var algo = digestAlgorithmMap[algoId];
  if (!algo) {
    throw new Error('algorithm is unknown:' + signerInfo.digestAlgorithm);
  }

  var source = new Uint8Array(der.toHex().length/2);
  forge.util.binary.hex.decode(der.toHex(), source, 0);
  var hash = window.crypto.subtle.digest({
    name: algo},
  source);

  return hash.then(function(firstHash) {
    return window.crypto.subtle.digest({
      name: algo,
    }, firstHash);
  });
}

SIM.prototype.validate = function(sii, siiType, password) {
  var self = this;
  return new Promise(function(resolve, reject) {
    createPepsi(sii, siiType, password, self.authorityRandom, self.algoId).then(function(hash) {
      var pepsi = forge.util.binary.hex.encode(self.pepsi.bytes());
      var hashHex = forge.util.binary.hex.encode(hash);
      if (pepsi === hashHex) {
        resolve();
      } else {
        reject(false);
      }
    }).catch(function(error) {
      reject(error);
    });
  });
}

module.exports = SIM;
