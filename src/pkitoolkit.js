'use strict';

/**
 * PkiToolkit interface
 * @constructor
 * @param {Object} params = Parameters to the toolkit
 * @param {Boolean} params.useTls - Use the encrypted connection
 * @param {Number} params.port - Use a specific port
 */
var Toolkit = function(params) {
  var self = this;
  var useTls = params.useTls || false;
  var port = params.port || 18181;
  if (useTls) {
    port = params.port || 18182;
  }

  if (useTls) {
    self.url = "https://pkitoolkit.local:" + port;
  } else {
    self.url = "http://localhost:" + port;
  }
}

/**
 * Brings up key selection dialog
 * @param {function} cb - Callback function. The first parameter is err which is null if no error happened. This is an optional parameter.
 */
Toolkit.prototype.askKey = function(cb) {
  var self = this;
  var xhr = new XMLHttpRequest();
  xhr.onerror = function() {
    if (cb && typeof(cb) === 'function') {
      cb(new Error('unable to connect to PKI Toolkit'));
    }

  }
  xhr.onload = function() {
    if (cb && typeof(cb) === 'function') {
      cb(null);
    }
  }
  xhr.open("GET", self.url + "/sign");
  xhr.responseType = "document";
  xhr.send();
}

/**
 * Signs a file with selected key
 * @param {String} formName - Name of the form in the HTML
 * @param {String} fileElementId - The id of the file element holding the file
 * @param {function} cb - Callback function with an {ArrayBuffer} as it's second parameter holding the contents of the signed PEM. The first parameter is err which is null if no error happened. This is an optional parameter. If it is left empty, the file will be donwloaded by the browser.
 */
Toolkit.prototype.sign = function(formName, fileElementId, cb) {
  var self = this;

  var el = document.getElementById(fileElementId);
  if (!el.value) {
    if (cb && typeof(cb) === 'function') {
      cb(new Error('No file is selected'));
    } else {
      return;
    }
  }

  var form = document.querySelector(formName);
  var formData = new FormData(form)

  var request = new XMLHttpRequest();
  request.onerror = function() {
    if (cb && typeof(cb) === 'function') {
      cb(new Error('unable to connect to PKI Toolkit'));
    }
  }

  request.onload = function(evt) {
    if (evt && evt.target && evt.target.status === 400) {
      self.askKey();
      return;
    }

    var pos = el.value.lastIndexOf('\\');
    var fileName = el.value.substr(pos + 1);
    var ab = evt.target.response;
    if (cb && typeof(cb) === 'function') {
      cb(null, ab);
    } else {
      window.PKIWebSDK.Utils.toFile(ab, fileName + ".signed.pem", "application/x-pem-file");
    }
  }

  request.responseType = "arraybuffer";
  request.open("POST", self.url + "/sign");
  request.send(formData);
}

/**
 * Registers a user and install the certificate
 * @param {String} reference - The EJBCA reference
 * @param {String} secret - The EJBCA secret
 * @param {String} sii - The virtual ID
 * @param {String} p12Password - The password to protect the installed P12
 * @param {function} cb - Callback function. It will indicate whether the registration is successfull or not
 */
Toolkit.prototype.register = function(reference, secret, sii, p12password, cb) {
  var self = this;

  var request = new XMLHttpRequest();
  request.onerror = function() {
    if (cb && typeof(cb) === 'function') {
      cb(new Error('unable to connect to PKI Toolkit'));
    }
  }

  request.onload = function(evt) {
    if (evt && evt.target && evt.target.status === 400) {
      cb(new Error('Invalid parameters'));
      return;
    }
    if (evt && evt.target && evt.target.status === 500) {
      cb(new Error('Certificate could not be generated. Check EJBCA whether the certificate is already generated or not'));
      return;
    }

    if (cb && typeof(cb) === 'function') {
      cb(null);
    }
  }

  var params = "reference=" + reference + "&secret=" + secret + "&sii=" + sii + "&p12-password=" + p12password;
  request.open("POST", self.url + "/register", true);
  request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  request.send(params);
}


module.exports = Toolkit;
